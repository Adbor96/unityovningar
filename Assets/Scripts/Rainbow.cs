﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rainbow : MonoBehaviour
{
    Material material;
    public List<Color> colors = new List<Color>();
    int colorInt = 0;
    public float timerCounter;
    float timer = 1;
    // Start is called before the first frame update
    void Start()
    {
        material = GetComponent<MeshRenderer>().material;
        material.color = colors[colorInt];
    }

    // Update is called once per frame
    void Update()
    {
        material = GetComponent<MeshRenderer>().material;
        timer -= 1.0f * Time.deltaTime;
        Debug.Log("Timer "+ timer);
        if (timer <= 0)
        {
            colorInt++;
            material.color = colors[colorInt];
            Debug.Log("Color " + colorInt);
            timer = 1;
            if (colorInt == colors.Count -1)
            {
                colorInt = 0;
                material.color = colors[colorInt];
            }
        }
    }
}
