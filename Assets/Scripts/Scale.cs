﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scale : MonoBehaviour
{
    public float speed;
    public bool canDecrease;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.S))
        {
            transform.localScale += Vector3.one * speed * Time.deltaTime;
            canDecrease = true;
        }
        if (Input.GetKey(KeyCode.R) && canDecrease)
        {
            transform.localScale -= Vector3.one * speed * Time.deltaTime;
            if (transform.localScale.x <= 0.1f)
            {
                canDecrease = false;
            }
        }
    }
}
