﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lamp : MonoBehaviour
{
    public bool lightIsOn = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Light myLight = GetComponent<Light>();

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (!lightIsOn)
            {
                lightIsOn = true;
                myLight.intensity = 30;
            }
            else
            {
                lightIsOn = false;
                myLight.intensity = 0;
            }
        }
    }
}
