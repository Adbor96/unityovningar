﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseIntensity : MonoBehaviour
{
    public float increaseSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Light myLight = GetComponent<Light>();
        myLight.intensity += increaseSpeed * Time.deltaTime;
    }
}
