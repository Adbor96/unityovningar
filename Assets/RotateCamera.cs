﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCamera : MonoBehaviour
{
    public float speed;

    float xRotation = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float xRotation = Input.GetAxis("Mouse X") * speed * Time.deltaTime;
        float yRotation = Input.GetAxis("Mouse Y") * speed * Time.deltaTime;

        if (Input.GetAxis("Mouse X") > 0)
        {
            transform.Rotate(Vector3.up * speed * Time.deltaTime);
        }
        if (Input.GetAxis("Mouse X") < 0)
        {
            transform.Rotate(Vector3.up * -speed * Time.deltaTime);
        }
    }
}
